<?php
/**
 * @file
 * Administration and checking functions.
 */

/**
 * @return array
 */
function uniquetitle_entity_types_with_title() {
  static $entity_types = NULL;

  if ($entity_types === NULL) {
    $entity_types = array();
    foreach (entity_get_info() as $entity_type => $entity_info) {
      if (!empty($entity_info['base table'])
        && !empty($entity_info['entity keys']['label'])
      ) {
        $entity_types[$entity_type] = $entity_info;
      }
    }

    // @todo Move this to a centralized place.
    // User name is already unique by it's nature.
    unset($entity_types['user'], $entity_types['rules_config']);
  }

  return $entity_types;
}

/**
 * FormAPI form builder callback.
 */
function uniquetitle_settings_bundles_form($form, &$form_state) {
  $entity_types = uniquetitle_entity_types_with_title();

  $path = drupal_get_path('module', 'uniquetitle');
  $title_separator = ' > ';
  $default = uniquetitle_default_bundle_settings();

  $entity_type_weights = array();

  $form['entity_types'] = array(
    '#theme_wrappers' => array('uniquetitle_tabs'),
    '#attached' => array(
      'js' => array(
        "$path/js/uniquetitle.js",
      ),
      'library' => array(
        array('system', 'ui.tabs'),
      ),
    ),
  );
  foreach ($entity_types as $entity_type => $entity_info) {
    if (empty($entity_info['bundles'])) {
      continue;
    }

    $entity_type_weights[$entity_type] = $entity_info['label'];

    $form['entity_types'][$entity_type] = array(
      '#entity_tyle' => $entity_type,
      '#title' => $entity_info['label'],
      '#theme_wrappers' => array('uniquetitle_tabs_tab'),
      '#uniquetitle_tabs_tab_attributes' => array(
        'id' => $entity_type,
      ),
      'bundles' => array(
        '#entity_type' => $entity_type,
        '#theme' => 'uniquetitle_settings_bundles_properties',
        '#attached' => array(
          'css' => array(
            "$path/css/uniquetitle.css",
          ),
        ),
        '#header' => array(
          'unique' => array(
            'data' => t('Unique'),
          ),
          'case_sensitive' => array(
            'data' => t('Case sensitive'),
          ),
        ),
        '#columns' => array(
          'unique' => array('unique'),
          'case_sensitive' => array('case_sensitive'),
        ),
      ),
    );

    $revision_capable = !empty($entity_info['revision table']);
    if ($revision_capable) {
      $form['entity_types'][$entity_type]['bundles']['#header']['recycle'] = array(
        'recycle' => t('Recycle'),
      );
      $form['entity_types'][$entity_type]['bundles']['#columns']['recycle'] = array(
        'recycle',
      );
    }

    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      $key = "uniquetitle_bundle__{$entity_type}__{$bundle_name}";
      $title_prefix = $entity_info['label'] . $title_separator . $bundle_info['label'];
      $settings = variable_get($key, $default);
      $states = array(
        'invisible' => array(
          ":input[name=\"{$key}[unique]\"]" => array('checked' => FALSE),
        ),
      );
      $form['entity_types'][$entity_type]['bundles'][$bundle_name][$key] = array(
        '#tree' => TRUE,
        '#title' => $bundle_info['label'],
        '#entity_type' => $entity_type,
        '#bundle_name' => $bundle_name,
        'unique' => array(
          '#type' => 'checkbox',
          '#title' => $title_prefix . $title_separator . t('Unique'),
          '#default_value' => $settings['unique'],
        ),
        'case_sensitive' => array(
          '#type' => 'checkbox',
          '#title' => $title_prefix . $title_separator . t('Case sensitive'),
          '#default_value' => $settings['case_sensitive'],
          '#states' => $states,
        ),
        'recycle' => array(
          '#type' => 'radios',
          '#required' => TRUE,
          '#title' => t('Recycle'),
          '#default_value' => $settings['recycle'],
          '#options' => uniquetitle_revision_options(),
          '#access' => $revision_capable,
          '#states' => $states,
        ),
      );
    }
  }

  asort($entity_type_weights);
  foreach (array_keys($entity_type_weights) as $weight => $entity_type) {
    $form['entity_types'][$entity_type]['#weight'] = $weight;
  }

  return system_settings_form($form);
}

/**
 * Form API form #validate callback.
 *
 * @see uniquetitle_settings_bundles_form()
 */
function uniquetitle_settings_bundles_form_validate(&$form, &$form_state) {
  $entity_types = uniquetitle_entity_types_with_title();
  foreach ($entity_types as $entity_type => $entity_info) {
    if (empty($entity_info['bundles'])) {
      continue;
    }

    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      $key = "uniquetitle_bundle__{$entity_type}__{$bundle_name}";
      if (!isset($form_state['values'][$key]) || empty($form_state['values'][$key]['unique'])) {
        continue;
      }

      $duplications = uniquetitle_get_title_duplications($entity_type, $bundle_name, $form_state['values'][$key]);
      if (count($duplications)) {
        form_set_error($key, 'Something wrong');
      }
    }
  }
}

/**
 * @param array $duplications
 * @param string $type_name
 *
 * @return array
 */
function uniquetitle_get_title_duplications_item_list($duplications, $type_name) {
  $max_title = 2;
  $max_node = 2;
  $count_dup = count($duplications);
  $to_dup = min($max_title, $count_dup);

  $items = array(
    'data'     => t('Title duplications in content type %type. Cannot be set to unique.', array('%type' => $type_name)),
    'children' => array(),
  );
  for ($i = 0; $i < $to_dup; $i++) {
    $current_t = each($duplications);
    $items['children'][$i]['data'] = $current_t['key'];
    $to_node = min($max_node, count($current_t['value']));
    for ($x = 0; $x < $to_node; $x++) {
      $current_n = each($current_t['value']);
      $items['children'][$i]['children'][] = l($current_n['value']->title, "node/{$current_n['value']->nid}", array('attributes' => array('target' => '_blank')));
    }

    if ($to_node < count($current_t['value'])) {
      $items['children'][$i]['children'][] = '...';
    }
  }

  if ($to_dup < $count_dup) {
    $items['children'][] = '...';
  }

  return $items;
}

/**
 * @param $entity_type
 * @param $bundle_name
 * @param array $settings
 *
 * @return array
 */
function uniquetitle_get_title_duplications($entity_type, $bundle_name, array $settings = array()) {
  if (!$settings) {
    $settings = variable_get("uniquetitle_bundle__{$entity_type}__{$bundle_name}", uniquetitle_default_bundle_settings());
  }

  $entity_info = entity_get_info($entity_type);
  $table = $entity_info['base table'];

  $id_key = $entity_info['entity keys']['id'];
  $label_key = $entity_info['entity keys']['label'];
  $bundle_key = $entity_info['entity keys']['bundle'];
  $language_key = $entity_info['entity keys']['language'];

  $join_conditions = array(
    "b1.{$bundle_key} = %alias.{$bundle_key}",
    "b1.{$id_key} <> %alias.{$id_key}",
    "b1.{$label_key} = %alias.{$label_key}",
  );
  if ($language_key) {
    $join_conditions[] = "b1.{$language_key} = b2.{$language_key}";
  }

  $query = db_select($table, 'b1');
  $query->innerJoin($table, 'b2', implode(' AND ', $join_conditions));
  $query->fields('b1', array($id_key, $label_key, $language_key));
  $query->condition("b1.{$bundle_key}", $bundle_name);

  $return = array();
  foreach ($query->execute() as $row) {
    $label = drupal_strtolower($row->$label_key);
    $return[$label][$row->$id_key] = $row;
  }

  return $return;
}

/**
 * @param EntityDrupalWrapper $entity
 * @param array $settings
 *
 * @return EntityDrupalWrapper|false
 */
function uniquetitle_title_exists(EntityDrupalWrapper $entity, array $settings = array()) {
  $entity_info = $entity->entityInfo();
  $entity_type = $entity->type();
  $bundle_name = $entity->getBundle();
  if (!$settings) {
    $settings = variable_get("uniquetitle_bundle__{$entity_type}__{$bundle_name}", uniquetitle_default_bundle_settings());
  }

  // @todo Revision management.
  $table = $entity_info['base table'];

  $id_key = $entity->entityKey('id');
  $label_key = $entity->entityKey('label');
  $bundle_key = $entity->entityKey('bundle');
  $language_key = $entity->entityKey('language');

  $query = db_select($table, 't')
    ->fields('t', array($id_key))
    ->condition("t.{$bundle_key}", $bundle_name)
    // @todo Case sensitive.
    ->condition("t.{$label_key}", $entity->label())
    ->range(0, 1);

  if ($language_key) {
    $query->condition("t.{$language_key}", $entity->$language_key->value());
  }

  $entity_id = $entity->getIdentifier();
  if ($entity_id) {
    $query->condition("t.{$id_key}", $entity_id, '<>');
  }

  $exists_id = $query->execute()->fetchColumn();

  return $exists_id ? entity_metadata_wrapper($entity->type(), $exists_id) : FALSE;
}

/**
 * Case sensitive operator.
 *
 * The MySQL and the PostgreSQL has different syntax for the
 * case sensitive operator.
 *
 * @param string $f1
 * @param string $f2
 * @param string $operator
 *
 * @return string|null
 */
function uniquetitle_db_binary_operator($f1, $f2, $operator = '=') {
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      return "BINARY $f1 $operator $f2";

    case 'pgsql':
      return "{$f1}::bytea $operator {$f2}::bytea";
  }

  // @todo Support other DB engines.
  return NULL;
}
