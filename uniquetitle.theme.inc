<?php
/**
 * @file
 * Theme layer related functions.
 */

/**
 * @ingroup themeable
 */
function theme_uniquetitle_tabs($vars) {
  $vars['element']['#uniquetitle_tabs_attributes']['class'][] = 'uniquetitle-tabs';
  $open = '<div' . drupal_attributes($vars['element']['#uniquetitle_tabs_attributes']) . '>';
  $close = '</div>';

  $navigation = '<ul>';
  foreach (element_children($vars['element']) as $key) {
    $id = $vars['element'][$key]['#uniquetitle_tabs_tab_attributes']['id'];
    $navigation .= "<li><a href=\"#{$id}\">" . $vars['element'][$key]['#title'] . '</a></li>';
  }

  $navigation .= '</ul>';

  return $open . $navigation . $vars['element']['#children'] . $close;
}

/**
 * @ingroup themeable
 */
function theme_uniquetitle_tabs_tab($vars) {
  $open = '<div' . drupal_attributes($vars['element']['#uniquetitle_tabs_tab_attributes']) . '>';
  $close = '</div>';

  return $open . $vars['element']['#children'] . $close;
}

/**
 * @ingroup themeable
 */
function theme_uniquetitle_settings_bundles_properties($vars) {
  $vars['element']['#attributes']['class'][] = 'uniquetitle-bundles-properties';
  if (isset($vars['element']['#columns']['recycle'])) {
    $vars['element']['#attributes']['class'][] = 'recycle';
  }

  $table = array(
    'sticky' => TRUE,
    'header' => array(
      'bundle' => array('data' => t('Bundle')),
    ) + $vars['element']['#header'],
    'rows' => array(),
    'attributes' => $vars['element']['#attributes'],
    'colgroups' => array(
      'bundle' => array(
        array(
          'class' => array('bundle'),
        ),
      ),
      'unique' => array(
        array(
          'class' => array('unique'),
        ),
      ),
      'case_sensitive' => array(
        array(
          'class' => array('case-sensitive'),
        ),
      ),
    ),
  );

  $entity_type = $vars['element']['#entity_type'];
  $entity_info = entity_get_info($entity_type);
  foreach (element_children($vars['element']) as $bundle_name) {
    $table['rows'][$bundle_name] = array(
      'data' => array(),
    );

    $key = "uniquetitle_bundle__{$entity_type}__{$bundle_name}";
    $table['rows'][$bundle_name]['data']['bundle'] = array(
      'data' => $entity_info['bundles'][$bundle_name]['label'],
    );

    foreach ($vars['element']['#columns'] as $column_name => $fields) {
      $table['rows'][$bundle_name]['data'][$column_name]['data'] = array();

      foreach ($fields as $field_name) {
        $vars['element'][$bundle_name][$key][$field_name]['#title_display'] = 'none';
        $table['rows'][$bundle_name]['data'][$column_name]['data'][] = render(
          $vars['element'][$bundle_name][$key][$field_name]
        );
      }

      $table['rows'][$bundle_name]['data'][$column_name]['data'] = implode(
        '',
        $table['rows'][$bundle_name]['data'][$column_name]['data']
      );
    }
  }

  return theme('table', $table);
}
