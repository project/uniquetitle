/**
 * @file
 * Unique title related behaviours.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.uniquetitleTabs = {
    attach: function (context, settings) {
      $('.uniquetitle-tabs:not(.uniquetitle-tabs-processed)', context)
        .addClass('uniquetitle-tabs-processed')
        .tabs();
    },
    detach: function (context, settings) {
      $('.uniquetitle-tabs.uniquetitle-tabs-processed', context)
        .removeClass('uniquetitle-tabs-processed')
        .tabs('destroy');
    }
  };
})(jQuery);
